The simplest way to contribute to adacraft studio is to open the project with Gitpod:

<a href="https://gitpod.io/#https://gitlab.com/adacraft/scratch-mod/main">
  <img
    src="https://img.shields.io/badge/Contribute%20with-Gitpod-908a85?logo=gitpod"
    alt="Contribute with Gitpod"
  />
</a>

# Main entry point for the Scratch mod for adacraft

This repository gathers information about the adacraft studio (or editor) which
is a Scratch mod (in fact it is baszed on TurboWarp which is another Scratch
mod)

You need all the "Scratch *" projects of this group to make it work (i.e.
`scratch-gui`, `scratch-vm`, etc.)

This repo also centralize issues that are common to several Scrach mod repos.

## Instructions

Here are the instructions to get the code, work on it, and build it.

### Get the code

Go to your working directory on your computer (something like
`adacraft/scratch-mod`), and get all the adacraft repositories:

```
git clone git@gitlab.com:adacraft/scratch-mod/scratch-blocks.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-gui.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-l10n.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-paint.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-render.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-svg-renderer.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-vm.git
```

### Prepare the code

Be sure you have Java 11 installed. If you're on a Ubuntu like OS you can
install it with the following commands:

```
apt-get update && apt-get install -y openjdk-11-jdk
```

The next step is to go in each subdirectory, and install everything:

```
( cd scratch-blocks && npm install )
( cd scratch-l10n && npm install && npm run build )
( cd scratch-paint && npm install )
# Order is important here. There are some errors if we build "render" before
# "svg-renderer".
( cd scratch-svg-renderer && npm install )
( cd scratch-render && npm install )
( cd scratch-vm && npm install )
( cd scratch-gui && npm install )
```

### Work on the code

At last, go to `scratch-gui` and execute this command:

```
npm run start
```

This will launch a live dev version of the editor with hot reload. Open it with this link http://localhost:8601.

You can now modify the code, and see what you're creating.

### Build

To build adacraft editor, go to the `scratch-gui` directory, and then execute
the following command.

```
npm run build
```

The result of the build is in the `build` directory.

To build for production, run this command:

```
NODE_ENV=production npm run build
```
